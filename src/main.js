import Vue from 'vue'
import App from './App.vue'
import router from './router'
import $ from 'jquery'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'


import './assets/css/bootstrap.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/material-dashboard.scss'
import './assets/css/styles.scss'
import './assets/css/file-upload.scss'
import './assets/font/flaticon.scss'


import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use($);

new Vue({
  el: '#app',
  router,
  $,
  render: h => h(App)
})
