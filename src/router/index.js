import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    scrollBehavior() {
        return window.scrollTo({ top: 0, behavior: 'smooth' });
    },
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
            component: () => import('@/components/Base.vue'),
            children: [
                {
                    path: "/dashboard",
                    name: "dashboard",
                    
                },
                {
                    path: "/event",
                    redirect: 'list-event',
                    component: () => import("@/components/Event/Event.vue"),
                    children: [
                        {
                            path: "list",
                            name: 'event.list',
                            component: () => import("@/components/Event/ListEvent.vue"),
                        },
                        {
                            path: "create",
                            name: "event.create",
                            component: () => import("@/components/Event/CreateEvent.vue")
                        },
                    ]
                },

            ]
        },

    ]
})